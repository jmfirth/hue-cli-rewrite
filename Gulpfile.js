var gulp = require('gulp');
var babel = require('gulp-babel');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');

gulp.task('babel', function() {
  return gulp.src(['src/bin/hue','src/**/*.es6'])
    .pipe(plumber())
    .pipe(babel())
    .pipe(rename(function(path) {
      // src/bin/hue -> bin/hue
			if (path.dirname === '.' && path.basename === 'hue') {
				path.dirname = 'bin';
				path.extname = '';
			}
      // src/**/*.es6 -> **/*.js
			else if (path.extname === '.es6') {
				path.extname = '.js';
			}
		}))
    .pipe(gulp.dest('./'));
});

gulp.task('watch', function() {
  gulp.watch(['src/bin/hue','src/**/*'], ['babel']);
});

// Default Task
gulp.task('default', ['babel']);
