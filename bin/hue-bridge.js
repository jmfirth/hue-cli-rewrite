#!/usr/bin/env node

'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _readlineSync = require('readline-sync');

var _readlineSync2 = _interopRequireDefault(_readlineSync);

var _libBridgeService = require('../lib/bridge-service');

var _libBridgeService2 = _interopRequireDefault(_libBridgeService);

var bridgeService = new _libBridgeService2['default']();

_commander2['default'].version('0.1.0').alias('hue bridge').description('List, select and register your bridge');

_commander2['default'].command('list').description('List available Philips Hue bridges').action(function () {
	bridgeService.all().then(function (bridges) {
		console.log();
		for (var i = 0; i < bridges.length; i++) {
			var bridge = bridges[i];
			console.log('[%s] %s', i + 1, bridge.ipaddress);
		}
		return bridges;
	});
});

_commander2['default'].command('select [ip]').description('Select a bridge').action(function (ip) {
	if (!!ip) {
		bridgeService.select(ip);
		return;
	}
	bridgeService.all().then(function (bridges) {
		var ips = [];
		bridges.map(function (bridge) {
			ips.push(bridge.ipaddress);
		});
		var index = _readlineSync2['default'].keyInSelect(ips, 'Choose bridge: ');
		bridgeService.select(ips[index]);
	});
});

_commander2['default'].command('register [username] [description]').description('Register to a Philips Hue bridge').action(function (username, description) {
	if (!username) {
		username = _readlineSync2['default'].question('User name [default: auto]: ');
		if (!username) username = null;
	}
	if (!description) {
		description = _readlineSync2['default'].question('User description [default: auto]: ');
		if (!description) description = null;
	}
	bridgeService.register(username, description);
});

_commander2['default'].command('check <username>').description('Connect to the bridge').action(function (username) {
	bridgeService.check(username);
});

_commander2['default'].parse(process.argv);