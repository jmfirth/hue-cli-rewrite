#!/usr/bin/env node

'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _libRangeParser = require('../lib/range-parser');

var _libRangeParser2 = _interopRequireDefault(_libRangeParser);

var _libStateService = require('../lib/state-service');

var _libStateService2 = _interopRequireDefault(_libStateService);

var _libLightService = require('../lib/light-service');

var _libLightService2 = _interopRequireDefault(_libLightService);

var rangeParser = new _libRangeParser2['default']();
var stateService = new _libStateService2['default']();
var lightService = new _libLightService2['default']();

_commander2['default'].version('0.1.0').alias('hue lights').usage('<pattern...> [options]').description('Turn lights on or off, change color and brightness').option('-n --on', 'turn lights on').option('-f --off', 'turn lights off').option('-c --color <color>', 'set color (color name or hex value)').option('-b --brightness <brightness>', 'set brightness (0-100)').option('-r --rgb <rgb>', 'set RGB color value (e.g. 255,255,255 for white)').parse(process.argv);

var patterns = _commander2['default'].args;

var lights = [];
patterns.map(function (pattern) {
	// union two arrays with splats
	lights = [].concat(_toConsumableArray(new Set([].concat(_toConsumableArray(lights), _toConsumableArray(rangeParser.parse(pattern))))));
});

var state = stateService.parse(_commander2['default'].hasOwnProperty('on'), _commander2['default'].hasOwnProperty('off'), _commander2['default'].color, _commander2['default'].rgb, _commander2['default'].brightness);

lightService.set(lights, state);