#!/usr/bin/env node

'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _promptly = require('promptly');

var _promptly2 = _interopRequireDefault(_promptly);

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var _libRangeParser = require('../lib/range-parser');

var _libRangeParser2 = _interopRequireDefault(_libRangeParser);

var _libGroupService = require('../lib/group-service');

var _libGroupService2 = _interopRequireDefault(_libGroupService);

var _libStateService = require('../lib/state-service');

var _libStateService2 = _interopRequireDefault(_libStateService);

var promptlyPrompt = _promise2['default'].denodeify(_promptly2['default'].prompt);
var rangeParser = new _libRangeParser2['default']();
var groupService = new _libGroupService2['default']();
var stateService = new _libStateService2['default']();

_commander2['default'].version('0.1.0').alias('hue group').description('Create, manage, and control groups');

_commander2['default'].command('list').description('List available groups').action(function () {
	groupService.list();
});

_commander2['default'].command('create <group_name> [pattern]').description('Create a new group').action(function (name, pattern) {
	if (!pattern) promptlyPrompt('Choose lights:').then(function (range) {
		var lights = rangeParse.parse(range);
		groupService.create(name, lights);
	});
});

_commander2['default'].command('update <group_name> [pattern]').description('Update an existing group').action(function (name, pattern) {
	var lights = [];
	if (!!pattern) {
		lights = rangeParser.parse(lights);
	}
	if (lights.length === 0) {
		promptlyPrompt('Choose lights:').then(function (lights) {
			var lights = rangeParse.parse(range);
			groupService.updateGroup(name, lights);
		});
	}
	groupService.updateGroup(name, lights);
});

_commander2['default'].command('state <group_name>').description('Modify the group light state').option('-n --on', 'turn lights on').option('-f --off', 'turn lights off').option('-c --color <color>', 'set color (color name or hex value)').option('-b --brightness <brightness>', 'set brightness (0-100)').option('-r --rgb <rgb>', 'set RGB color value (e.g. 255,255,255 for white)').action(function (name, options) {
	var state = stateService.parse(options.hasOwnProperty('on'), options.hasOwnProperty('off'), options.color, options.rgb, options.brightness);
	groupService.set(name, state);
});

_commander2['default'].parse(process.argv);