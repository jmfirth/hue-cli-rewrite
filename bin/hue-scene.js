#!/usr/bin/env node

'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _promptly = require('promptly');

var _promptly2 = _interopRequireDefault(_promptly);

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var _libSceneService = require('../lib/scene-service');

var _libSceneService2 = _interopRequireDefault(_libSceneService);

var promptlyPrompt = _promise2['default'].denodeify(_promptly2['default'].prompt);
var sceneService = new _libSceneService2['default']();

_commander2['default'].version('0.1.0').alias('hue scene').description('Create, manage, and control scenes');

_commander2['default'].command('list').description('List available scenes').action(function () {
	sceneService.list();
});

_commander2['default'].command('save <scene_name>').description('Save a scene').action(function (name) {
	sceneService.save(name);
});

_commander2['default'].command('load <scene_name>').description('Load a scene').action(function (name) {
	sceneService.load(name);
});

_commander2['default'].parse(process.argv);