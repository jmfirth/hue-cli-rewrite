'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeHueApi = require('node-hue-api');

var _nodeHueApi2 = _interopRequireDefault(_nodeHueApi);

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var HueApi = _nodeHueApi2['default'].HueApi;

var LightService = (function () {
	function LightService() {
		_classCallCheck(this, LightService);

		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}

	_createClass(LightService, [{
		key: 'list',
		value: function list() {
			return this.api.lights().then(function (lights) {
				console.dir(lights);
				console.log('\nTotal number of lights: ', lights.length);
			})['catch'](this.error);
		}
	}, {
		key: 'set',
		value: function set(lights, state) {
			var self = this;
			return lights.reduce(function (previous, light) {
				return previous.then(function () {
					return self.api.setLightState(light, state);
				});
			}, _promise2['default'].resolve())['catch'](this.error);
		}
	}, {
		key: 'error',
		value: function error(message) {
			if (!message) return;
			console.log('Error:', message);
			throw '';
		}
	}]);

	return LightService;
})();

exports['default'] = LightService;
module.exports = exports['default'];