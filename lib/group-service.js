'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeHueApi = require('node-hue-api');

var _nodeHueApi2 = _interopRequireDefault(_nodeHueApi);

var HueApi = _nodeHueApi2['default'].HueApi;

var GroupService = (function () {
	function GroupService() {
		_classCallCheck(this, GroupService);

		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}

	// get a group

	_createClass(GroupService, [{
		key: 'get',
		value: function get(name) {
			var self = this;
			return this.api.groups().then(function (groups) {
				for (var index in groups) {
					var group = groups[index];
					if (group.id === name || group.name === name) return group;
				}
				if (!group) {
					console.log('Group not found');
					return;
				}
				throw 'Group not found';
			});
		}

		// set the group to a state
	}, {
		key: 'set',
		value: function set(name, state) {
			var self = this;
			return this.get(name).then(function (group) {
				return group.lights.reduce(function (previous, light) {
					return previous.then(function () {
						return self.api.setLightState(light, state);
					}, function (error) {
						console.log('>> failed to set light ', light);
					});
				}, Promise.resolve());
			})['catch'](this.error);
		}

		// list available groups
	}, {
		key: 'list',
		value: function list() {
			return this.api.groups().then(function (groups) {
				console.dir(groups);
				console.log('\nTotal number of groups: ', groups.length);
			})['catch'](this.error);
		}

		// create group with lights
	}, {
		key: 'create',
		value: function create(name, lights) {
			return this.api.createGroup(name, lights)['catch'](this.error);
		}

		// update group lights
	}, {
		key: 'update',
		value: function update(name, lights) {
			return this.api.updateGroup(name, lights)['catch'](this.error);
		}
	}, {
		key: 'error',
		value: function error(message) {
			console.log('Error:', message);
			throw '';
		}
	}]);

	return GroupService;
})();

exports['default'] = GroupService;
module.exports = exports['default'];