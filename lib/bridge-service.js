'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeHueApi = require('node-hue-api');

var _nodeHueApi2 = _interopRequireDefault(_nodeHueApi);

var _libConfigService = require('../lib/config-service');

var _libConfigService2 = _interopRequireDefault(_libConfigService);

var configService = new _libConfigService2['default']();

var HueApi = _nodeHueApi2['default'].HueApi;

var BridgeService = (function () {
	function BridgeService() {
		_classCallCheck(this, BridgeService);

		this.ip = '192.168.1.10';
	}

	_createClass(BridgeService, [{
		key: 'all',
		value: function all() {
			return _nodeHueApi2['default'].nupnpSearch();
		}
	}, {
		key: 'select',
		value: function select(ip) {
			// TODO: store ip
		}
	}, {
		key: 'users',
		value: function users() {}
	}, {
		key: 'register',
		value: function register(username, description) {
			var self = this;
			return this.connect(username).then(function (registered) {
				if (registered) throw 'User is registered';
			}).then(function () {
				console.log('User is not registered. Press the link button');
				var api = new HueApi();
				return api.registerUser(self.ip, username, description);
			}).then(function (result) {
				console.dir(result);
			});
		}
	}, {
		key: 'connect',
		value: function connect(username) {
			var api = new HueApi(this.ip, username);
			return api.config().then(function (result) {
				return !!result.hasOwnProperty('ipaddress') && !!result.hasOwnProperty('linkbutton');
			});
		}
	}, {
		key: 'check',
		value: function check(username) {
			var api = new HueApi(this.ip, username);
			return this.connect(username).then(function (registered) {
				if (!!registered) console.log('User "%s" is registered.', username);else console.log('User "%s" is not registered.', username);
			});
		}
	}, {
		key: 'error',
		value: function error() {
			if (!message) return;
			console.log('Error:', message);
			throw '';
		}
	}]);

	return BridgeService;
})();

exports['default'] = BridgeService;
module.exports = exports['default'];