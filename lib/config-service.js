'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nconf = require('nconf');

var _nconf2 = _interopRequireDefault(_nconf);

var _promise = require('promise');

var _promise2 = _interopRequireDefault(_promise);

var ConfigService = (function () {
	function ConfigService() {
		_classCallCheck(this, ConfigService);

		this.load();
	}

	_createClass(ConfigService, [{
		key: 'get',
		value: function get(name) {
			return _nconf2['default'].get(name);
		}
	}, {
		key: 'set',
		value: function set(name, value) {
			_nconf2['default'].set(name, value);
		}
	}, {
		key: 'load',
		value: function load() {
			_nconf2['default'].file('./.hue-cli.json');
		}
	}, {
		key: 'save',
		value: function save() {
			return _promise2['default'].denodeify(_nconf2['default'].save)();
		}
	}]);

	return ConfigService;
})();

exports['default'] = ConfigService;
module.exports = exports['default'];