'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeHueApi = require('node-hue-api');

var _nodeHueApi2 = _interopRequireDefault(_nodeHueApi);

var HueApi = _nodeHueApi2['default'].HueApi;

var SceneService = (function () {
	function SceneService() {
		_classCallCheck(this, SceneService);

		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}

	_createClass(SceneService, [{
		key: 'list',
		value: function list() {
			return this.api.scenes().then(function (scenes) {
				console.dir(scenes);
				console.log('\nTotal number of scenes: ', scenes.length);
			})['catch'](this.error);
		}
	}, {
		key: 'get',
		value: function get(name) {
			var self = this;
			return this.api.scenes().then(function (scenes) {
				for (var index in scenes) {
					var scene = scenes[index];
					if (scene.id === name || scene.name === name) return scene;
				}
				throw 'Scene not found';
			});
		}
	}, {
		key: 'load',
		value: function load(name) {
			var self = this;
			return this.get(name).then(function (scene) {
				return self.api.recallScene(scene.id);
			})['catch'](this.error);
		}
	}, {
		key: 'save',
		value: function save(name) {
			var self = this;
			var lights = [];
			var scene = null;
			return this.get(name)
			// store the current scene
			.then(function (result) {
				scene = result;
			})
			// retrieve the current state and build a list of lights
			.then(function () {
				return self.api.fullState().then(function (state) {
					for (var index in state.lights) {
						var light = state.lights[index];
						light.id = index;
						if (!!light.state.on) lights.push(light);
					}
					return lights;
				});
			})
			// save the current state of each light to the scene
			.then(function () {
				var chain = lights.reduce(function (previous, light) {
					return previous.then(function () {
						return self.api.setSceneLightState(scene.id, light.id, light.state);
					}, function (error) {
						console.log('>> failed to set light ', light.id);
					});
				}, Promise.resolve());
				return chain;
			})['catch'](this.error);
		}
	}, {
		key: 'error',
		value: function error(message) {
			if (!message) return;
			console.log('Error:', message);
			throw '';
		}
	}]);

	return SceneService;
})();

exports['default'] = SceneService;
module.exports = exports['default'];