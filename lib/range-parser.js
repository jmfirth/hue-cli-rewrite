'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeRange = require("node-range");

var _nodeRange2 = _interopRequireDefault(_nodeRange);

var RangeParser = (function () {
	function RangeParser() {
		_classCallCheck(this, RangeParser);
	}

	_createClass(RangeParser, [{
		key: 'parse',
		value: function parse(str) {
			var a = str.toString().split(',');
			var b = [];
			a.forEach(function (bit) {
				var split = bit.split('-');
				if (split.length === 1) {
					b.push(parseInt(split[0]));
					return;
				}
				var min = parseInt(split[0]);
				var max = parseInt(split[1]) + 1;
				(0, _nodeRange2['default'])(min, max).toArray().forEach(function (num) {
					b.push(num);
				});
			});
			return b;
		}
	}]);

	return RangeParser;
})();

exports['default'] = RangeParser;
module.exports = exports['default'];