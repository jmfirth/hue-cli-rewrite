'use strict';

Object.defineProperty(exports, '__esModule', {
	value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _nodeHueApi = require('node-hue-api');

var _nodeHueApi2 = _interopRequireDefault(_nodeHueApi);

var _colorService = require('./color-service');

var _colorService2 = _interopRequireDefault(_colorService);

var lightState = _nodeHueApi2['default'].lightState;
var colorService = new _colorService2['default']();

var StateService = (function () {
	function StateService() {
		_classCallCheck(this, StateService);
	}

	_createClass(StateService, [{
		key: 'parse',
		value: function parse(on, off, color, rgb, brightness) {
			var state = this.create();
			state = this.visibility(state, on, off);
			state = this.color(state, color, rgb);
			state = this.brightness(state, brightness);
			return state;
		}
	}, {
		key: 'create',
		value: function create() {
			return lightState.create();
		}
	}, {
		key: 'visibility',
		value: function visibility(state, on, off) {
			if (!!on)
				// lights on
				return state.on();else if (!!off)
				// lights off
				return state.off();
			// no change
			return state;
		}
	}, {
		key: 'color',
		value: function color(state, name, rgb) {
			// try to set color state using a friendly color name
			if (!!name) {
				var color = colorService.get(name);
				if (!!color.rgb) {
					return state.rgb(color.rgb.r, color.rgb.g, color.rgb.b);
				}
			}
			// try to set color state using rgb
			if (!!rgb) {
				var split = rgb.split(',');
				if (split.length === 3) {
					return state.rgb(split[0], split[1], split[2]);
				}
			}
			// no change
			return state;
		}
	}, {
		key: 'brightness',
		value: function brightness(state, _brightness) {
			if (!!_brightness && !!parseInt(_brightness)) return state.brightness(_brightness);
			return state;
		}
	}]);

	return StateService;
})();

exports['default'] = StateService;
module.exports = exports['default'];