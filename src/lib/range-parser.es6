'use strict';

import range from "node-range";

export default class RangeParser {
	parse(str) {
		var a = str.toString().split(',');
		var b = [];
		a.forEach(function(bit) {
			var split = bit.split('-');
			if (split.length === 1) {
				b.push(parseInt(split[0]));
				return;
			}
			var min = parseInt(split[0]);
			var max = parseInt(split[1]) + 1;
			range(min, max).toArray().forEach((num) => {
				b.push(num);
			});
		});
		return b;
	}
}
