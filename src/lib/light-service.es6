'use strict';

import nodeHueApi from 'node-hue-api';
import Promise from 'promise';

let HueApi = nodeHueApi.HueApi;

export default class LightService {
	constructor() {
		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}
	list() {
		return this.api
			.lights()
			.then((lights) => {
				console.dir(lights);
				console.log('\nTotal number of lights: ', lights.length);
			})
			.catch(this.error);
	}
	set(lights, state) {
		var self = this;
		return lights.reduce((previous, light) => {
			return previous.then(() => {
				return self.api.setLightState(light, state);
			});
		}, Promise.resolve())
			.catch(this.error);
	}
	error(message) {
		if (!message) return;
		console.log('Error:', message);
		throw '';
	}
}
