'use strict';

import nodeHueApi from 'node-hue-api';
import ColorService from './color-service';

let lightState = nodeHueApi.lightState;
let colorService = new ColorService();

export default class StateService {
	constructor() {

	}
	parse(on, off, color, rgb, brightness) {
		var state = this.create();
		state = this.visibility(state, on, off);
		state = this.color(state, color, rgb);
		state = this.brightness(state, brightness);
		return state;
	}
	create() {
		return lightState.create();
	}
	visibility(state, on, off) {
		if (!!on)
		// lights on
			return state.on();
		else if (!!off)
			// lights off
			return state.off();
		// no change
		return state;
	}
	color(state, name, rgb) {
		// try to set color state using a friendly color name
		if (!!name) {
			var color = colorService.get(name);
			if (!!color.rgb) {
				return state.rgb(color.rgb.r, color.rgb.g, color.rgb.b);
			}
		}
		// try to set color state using rgb
		if(!!rgb) {
			var split = rgb.split(',');
			if (split.length === 3) {
				return state.rgb(split[0], split[1], split[2]);
			}
		}
		// no change
		return state;
	}
	brightness(state, brightness) {
		if (!!brightness && !!parseInt(brightness))
			return state.brightness(brightness);
		return state;
	}
}
