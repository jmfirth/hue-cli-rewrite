import nodeHueApi from 'node-hue-api';
import ConfigService from '../lib/config-service';

let configService = new ConfigService();

let HueApi = nodeHueApi.HueApi;

export default class BridgeService {
	constructor() {
		this.ip = '192.168.1.10';
	}
	all() {
		return nodeHueApi
			.nupnpSearch();
	}
	select(ip) {
		// TODO: store ip
	}
	users() {

	}
	register(username, description) {
		var self = this;
		return this
			.connect(username)
			.then((registered) => {
				if (registered)
					throw 'User is registered';
			})
			.then(function() {
				console.log('User is not registered. Press the link button');
				var api = new HueApi();
				return api
					.registerUser(self.ip, username, description);
			})
			.then(function(result) {
				console.dir(result);
			});
	}
	connect(username) {
		var api = new HueApi(this.ip, username);
		return api
			.config()
			.then((result) => {
				return !!result.hasOwnProperty('ipaddress')
					&& !!result.hasOwnProperty('linkbutton');
			});
	}
	check(username) {
		var api = new HueApi(this.ip, username);
		return this
			.connect(username)
			.then((registered) => {
				if (!!registered)
					console.log('User "%s" is registered.', username);
				else
					console.log('User "%s" is not registered.', username);
			});
	}
	error() {
		if (!message) return;
		console.log('Error:', message);
		throw '';
	}
}
