import nconf from 'nconf';
import Promise from 'promise';

export default class ConfigService {
	constructor() {
		this.load();
	}
	get(name) {
		return nconf.get(name);
	}
	set(name, value) {
		nconf.set(name, value);
	}
	load() {
		nconf.file('./.hue-cli.json');
	}
	save() {
		return Promise.denodeify(nconf.save)();
	}
}
