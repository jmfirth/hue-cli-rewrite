'use strict';

import nodeHueApi from 'node-hue-api';

let HueApi = nodeHueApi.HueApi;

export default class SceneService {
	constructor() {
		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}
	list() {
		return this.api
			.scenes()
			.then((scenes) => {
				console.dir(scenes);
				console.log('\nTotal number of scenes: ', scenes.length);
			})
			.catch(this.error);
	}
	get(name) {
		var self = this;
		return this.api
			.scenes()
			.then((scenes) => {
				for(var index in scenes) {
					var scene = scenes[index];
					if (scene.id === name || scene.name === name)
						return scene;
				}
				throw 'Scene not found';
			});
	}
	load(name) {
		var self = this;
		return this.get(name)
			.then((scene) => {
				return self.api.recallScene(scene.id);
			})
			.catch(this.error);
	}
	save(name) {
		var self = this;
		var lights = [];
		var scene = null;
		return this.get(name)
			// store the current scene
			.then((result) => {
				scene = result;
			})
			// retrieve the current state and build a list of lights
			.then(() => {
				return self.api
					.fullState()
					.then((state) => {
						for (var index in state.lights) {
							var light = state.lights[index];
							light.id = index;
							if (!!light.state.on)
								lights.push(light);
						}
						return lights;
					})
			})
			// save the current state of each light to the scene
			.then(function() {
				var chain = lights.reduce((previous, light) => {
					return previous.then(() => {
						return self.api.setSceneLightState(scene.id, light.id, light.state);
					}, (error) => {
						console.log('>> failed to set light ', light.id);
					});
				}, Promise.resolve());
				return chain;
			})
			.catch(this.error);
	}
	error(message) {
		if (!message) return;
		console.log('Error:', message);
		throw '';
	}
}
