'use strict';

import nodeHueApi from 'node-hue-api';

let HueApi = nodeHueApi.HueApi;

export default class GroupService {
	constructor() {
		// get hue base station info
		this.ip = '192.168.1.10';
		this.username = '1239089dkj12asdfd';
		this.api = new HueApi(this.ip, this.username);
	}
	// get a group
	get(name) {
		var self = this;
		return this.api
			.groups()
			.then(function(groups) {
				for(var index in groups) {
					var group = groups[index];
					if (group.id === name || group.name === name)
						return group;
				}
				if (!group) {
					console.log('Group not found');
					return;
				}
				throw 'Group not found';
			});
	}
	// set the group to a state
	set(name, state) {
		var self = this;
		return this.get(name)
			.then((group) => {
			 	return group.lights.reduce((previous, light) => {
					return previous.then(function () {
						return self.api.setLightState(light, state);
					}, (error) => {
						console.log('>> failed to set light ', light);
					});
				}, Promise.resolve());
			})
			.catch(this.error);
	}
	// list available groups
	list() {
		return this.api
			.groups()
			.then((groups) => {
				console.dir(groups);
				console.log('\nTotal number of groups: ', groups.length);
			})
			.catch(this.error);
	}
	// create group with lights
	create(name, lights) {
		return this.api
			.createGroup(name, lights)
			.catch(this.error);
	}
	// update group lights
	update(name, lights) {
		return this.api
			.updateGroup(name, lights)
			.catch(this.error);
	}
	error(message) {
		console.log('Error:', message);
		throw '';
	}
}
