#!/usr/bin/env node
'use strict';

import program from 'commander';
import readlineSync from 'readline-sync';
import BridgeService from '../lib/bridge-service';

let bridgeService = new BridgeService();

program
	.version('0.1.0')
	.alias('hue bridge')
	.description('List, select and register your bridge');

program
	.command('list')
	.description('List available Philips Hue bridges')
	.action(() => {
		bridgeService
			.all()
			.then((bridges) => {
				console.log();
				for(var i = 0; i < bridges.length; i++) {
					var bridge = bridges[i];
					console.log('[%s] %s', (i + 1), bridge.ipaddress);
				}
				return bridges;
			});
	});

program
	.command('select [ip]')
	.description('Select a bridge')
	.action((ip) => {
		if (!!ip) {
			bridgeService.select(ip);
			return;
		}
		bridgeService
			.all()
			.then((bridges) => {
				var ips = [];
				bridges.map((bridge) => { ips.push(bridge.ipaddress); });
				var index = readlineSync.keyInSelect(ips, 'Choose bridge: ');
				bridgeService.select(ips[index]);
			});
	});

program
	.command('register [username] [description]')
	.description('Register to a Philips Hue bridge')
	.action((username, description) => {
		if (!username) {
			username = readlineSync.question('User name [default: auto]: ');
			if (!username) username = null;
		}
		if (!description) {
			description = readlineSync.question('User description [default: auto]: ')
			if (!description) description = null;
		}
		bridgeService.register(username, description);
	});

program
	.command('check <username>')
	.description('Connect to the bridge')
	.action((username) => {
		bridgeService.check(username);
	});

program.parse(process.argv);
