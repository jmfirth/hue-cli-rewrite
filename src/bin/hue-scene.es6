#!/usr/bin/env node
'use strict';

import program from 'commander';
import promptly from 'promptly';
import Promise from 'promise';
import SceneService from '../lib/scene-service';

let promptlyPrompt = Promise.denodeify(promptly.prompt);
let sceneService = new SceneService();

program
	.version('0.1.0')
	.alias('hue scene')
	.description('Create, manage, and control scenes');

program
	.command('list')
	.description('List available scenes')
	.action(() => {
		sceneService.list();
	});

program
	.command('save <scene_name>')
	.description('Save a scene')
	.action((name) => {
		sceneService.save(name);
	});

program
	.command('load <scene_name>')
	.description('Load a scene')
	.action((name) => {
		sceneService.load(name);
	});

program.parse(process.argv);
