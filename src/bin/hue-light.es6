#!/usr/bin/env node
'use strict';

import program from 'commander';
import RangeParser from '../lib/range-parser';
import StateService from '../lib/state-service';
import LightService from '../lib/light-service';

let rangeParser = new RangeParser();
let stateService = new StateService();
let lightService = new LightService();

program
	.version('0.1.0')
	.alias('hue lights')
	.usage('<pattern...> [options]')
	.description('Turn lights on or off, change color and brightness')
	.option('-n --on', 'turn lights on')
	.option('-f --off', 'turn lights off')
	.option('-c --color <color>', 'set color (color name or hex value)')
	.option('-b --brightness <brightness>', 'set brightness (0-100)')
	.option('-r --rgb <rgb>', 'set RGB color value (e.g. 255,255,255 for white)')
	.parse(process.argv);

var patterns = program.args;

var lights = [];
patterns.map(function(pattern) {
	// union two arrays with splats
	lights = [...new Set([...lights, ...rangeParser.parse(pattern)])];
});

var state = stateService.parse(
	program.hasOwnProperty('on'),
	program.hasOwnProperty('off'),
	program.color,
	program.rgb,
	program.brightness);

lightService.set(lights, state);
