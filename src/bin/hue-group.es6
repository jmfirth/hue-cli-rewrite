#!/usr/bin/env node
'use strict';

import program from 'commander';
import promptly from 'promptly';
import Promise from 'promise';
import RangeParser from '../lib/range-parser';
import GroupService from '../lib/group-service';
import StateService from '../lib/state-service';

let promptlyPrompt = Promise.denodeify(promptly.prompt);
let rangeParser = new RangeParser();
let groupService = new GroupService();
let stateService = new StateService();

program
	.version('0.1.0')
	.alias('hue group')
	.description('Create, manage, and control groups');

program
	.command('list')
	.description('List available groups')
	.action(() => {
		groupService.list();
	});

program
	.command('create <group_name> [pattern]')
	.description('Create a new group')
	.action((name, pattern) => {
		if (!pattern)
			promptlyPrompt('Choose lights:')
				.then((range) => {
					var lights = rangeParse.parse(range);
					groupService.create(name, lights);
				});
	});

program
	.command('update <group_name> [pattern]')
	.description('Update an existing group')
	.action((name, pattern) => {
		var lights = [];
		if (!!pattern) {
			lights = rangeParser.parse(lights);
		}
		if (lights.length === 0) {
			promptlyPrompt('Choose lights:')
				.then((lights) => {
					var lights = rangeParse.parse(range);
					groupService.updateGroup(name, lights);
				});
		}
		groupService.updateGroup(name, lights);
	});

program
	.command('state <group_name>')
	.description('Modify the group light state')
	.option('-n --on', 'turn lights on')
	.option('-f --off', 'turn lights off')
	.option('-c --color <color>', 'set color (color name or hex value)')
	.option('-b --brightness <brightness>', 'set brightness (0-100)')
	.option('-r --rgb <rgb>', 'set RGB color value (e.g. 255,255,255 for white)')
	.action((name, options) => {
		var state = stateService.parse(
				options.hasOwnProperty('on'),
				options.hasOwnProperty('off'),
				options.color,
				options.rgb,
				options.brightness);
		groupService.set(name, state);
	});

program.parse(process.argv);
